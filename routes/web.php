<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\YoutubeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [YoutubeController::class, 'index'])->name('home');
Route::get('/upload', [YoutubeController::class, 'upload'])->name('upload');
Route::get('/result', [YoutubeController::class, 'result'])->name('result');
Route::get('/watch/{id}', [YoutubeController::class, 'watch'])->name('watch');

Route::prefix("/debug")->group(function(){
    Route::get("/view/{view}", [App\Http\Controllers\DebugController::class, "simpleView"])->name("debug.view");
    Route::post("/view/{view}", [App\Http\Controllers\DebugController::class, "simpleResponse"]);
});
