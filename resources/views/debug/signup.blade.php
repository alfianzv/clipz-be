<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/signup.css')}}">
        <title>Clipz</title>

    </head>
    <body>

        <section class="signup">
            <div class="contain d-flex flex-column justify-content-center align-items-center">
                <img class="logo" src="{{asset('./assets/image/signup/clipz-logo.svg')}}" alt="">
                <div class="content d-flex flex-column justify-content-center align-items-center">
                    <h1>Sign Up</h1>
                    <span class="w-100 d-flex flex-row justify-content-center align-items-center">
                        <p>Have an account?</p><a href="">Login</a>
                    </span>

                    <div class="input-cont w-100 d-flex flex-column justify-content-center">
                        <label for="name">
                            Name
                            <input id="name" name="name" type="text" placeholder="Full Name">
                        </label>
                        <label for="username">
                            Username
                            <input id="username" name="username" type="text" placeholder="Username">
                        </label>
                        <label for="channelname">
                            Channel Name
                            <input id="channelname" name="channelname" type="text" placeholder="Channel Name">
                        </label>
                        <label for="dob">
                            Date of Born
                            <input id="dob" name="dob" type="date" placeholder="Date of Born">
                        </label>
                        <label for="email">
                            Email
                            <input id="email" name="email" type="email" placeholder="john.doe@gmail.com">
                        </label>
                        <label for="password">
                            Password
                            <input id="password" name="password" type="password" placeholder="Password">
                        </label>
                        <label for="password_confirm">
                            Confirm Password
                            <input id="password_confirm" name="password_confirm" type="password" placeholder="Confirmation Password">
                        </label>
                    </div>

                    <button id="btnSubmit" class="btnSubmit" type="submit">
                        Sign Up
                    </button>
                </div>
            </div>
        </section>

        <img class="elp1" src="{{asset('./assets/image/signup/bg-ellips1.svg')}}" alt="">
        <img class="elp2" src="{{asset('./assets/image/signup/bg-ellips2.svg')}}" alt="">
        <script src="{{url('./js/app.js')}}"></script>
        <script src="{{url('./js/jquery-3.6.0.min.js')}}"></script>
        <script src="{{url('./js/signup.js')}}"></script>
    </body>
</html>
