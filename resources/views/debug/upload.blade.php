<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/upload.css')}}">
        <script src="{{url('./js/jquery-3.6.0.min.js')}}"></script>
        <script src="{{asset('js/home.js')}}"></script>
        <title>Clipz</title>
    </head>
    <body>
    <div class="container-header">
        <div class="header-left">
            <a href="{{url('/')}}">
                <div class="logo"></div>
            </a>    
            <div class="purple-text"><h>CLIPZ</h></div>
        </div>
        <div class="header-right">
            <a href="{{url('/upload')}}"><btn class="btn btn-primary">+ Upload Video</btn></a>
            <div class="dashboard">
                <div class="sub">
                    <div class="sub-navlink"><a>Home</a></div>
                    <div class="sub-navlink"><a>Upload</a></div>
                    <div class="sub-navlink"><a>Dashboard</a></div>
                </div>
            </div>
        </div>
     </div>
     <div class="container-content">
        <div class="container-upload">
            <div class="purple-text"><h1>Upload Video</h1></div>
            <div class="text">
                <div class="input-t">
                    <h>Title</h>
                    <input type="text" id="text-title">
                </div>
                <div class="input-d">
                    <h>Description</h>
                    <textarea id="text-description" name="description" rows="4" cols="50"></textarea>
                </div>
                <div class="input-f">
                    <input type="file" id="text-file">
                </div>
                <btn class="btn btn-primary" type="submit">Upload</btn>
            </div>
        </div>
    </div>
    </body>
</html>
