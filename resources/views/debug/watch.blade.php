<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/home.css')}}">
        <script src="{{url('./js/jquery-3.6.0.min.js')}}"></script>
        <script src="{{asset('js/home.js')}}"></script>
        <title>Clipz</title>
    </head>
    <body>
        <div class="container-header">
            <div class="header-left">
                <div class="logo"></div>
                <div class="purple-text"><h>CLIPZ</h></div>
            </div>
            <div class="header-right">
                <a href="{{url('/upload')}}"><btn class="btn btn-primary">+ Upload Video</btn></a>
                <div class="dashboard">
                    <div class="sub">
                        <div class="sub-navlink"><a>Home</a></div>
                        <div class="sub-navlink"><a>Upload</a></div>
                        <div class="sub-navlink"><a>Dashboard</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card mb-4" style="width: 100%">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe src="https://www.youtube.com/embed/{{ $singleVideo->items[0]->id }}" width="560" height="600" frameborder="0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="card-body">
                    <h5>{{ $singleVideo->items[0]->snippet->title }}</h5>
                    <p class="text-secondary">Published
                        at {{ date('d M Y', strtotime($singleVideo->items[0]->snippet->publishedAt)) }}</p>
                    <p>{{ $singleVideo->items[0]->snippet->description }}</p>
                </div>
            </div>
        </div>
        <!-- <button class="btn btn-danger">Button</button>
        <script src="{{url('./js/app.js')}}"></script> -->
    </body>
</html>