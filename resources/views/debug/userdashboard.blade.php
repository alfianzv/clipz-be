<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/userdashboard.css')}}">
        <title>Clipz</title>

    </head>
    <body>

        <section class="udb">
            <div class="contain d-flex flex-column justify-content-center align-items-center">
                <div class="w-100 header d-flex flex-row justify-content-between align-items-center">
                    <span class="d-flex flex-column justify-content-start align-items-start">
                        <img class="logo" src="{{asset('./assets/image/signup/clipz-logo.svg')}}" alt="">
                        <div class="bg-grey">
                            <h1>Uploaded Video</h1>
                        </div>
                    </span>
                    <button>
                        + Upload Video
                    </button>
                </div>
                <div class="content d-flex flex-column justify-content-center">
                    <div class="w-100">
                        <table>
                            <tr>
                                <th>Videos</th>
                                <th>Date</th>
                                <th>Views</th>
                                <th>Actions</th>
                            </tr>
                            <tr>
                                <td class="video">
                                    <img src="{{asset('./assets/image/userdashboard/ticket.png')}}" alt="">
                                    <span>
                                        <h3>
                                            Video Title
                                        </h3>
                                        <p>
                                            video description
                                        </p>
                                    </span>
                                </td>
                                <td>
                                    <p>17/08/2021</p>
                                </td>
                                <td>
                                    <p>109</p>
                                </td>
                                <td>
                                    <button class="del">
                                        Delete
                                    </button>
                                </td>
                            </tr>

                            
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <script src="{{url('./js/app.js')}}"></script>
        <script src="{{url('./js/jquery-3.6.0.min.js')}}"></script>
        <script src="{{url('./js/userdashboard.js')}}"></script>
    </body>
</html>
