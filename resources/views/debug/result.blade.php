<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/home.css')}}">
        <script src="{{url('./js/jquery-3.6.0.min.js')}}"></script>
        <script src="{{asset('js/home.js')}}"></script>
        <title>Clipz</title>
    </head>
    <body>
        <div class="container-header">
            <div class="header-left">
                <div class="logo"></div>
                <div class="purple-text"><h>CLIPZ</h></div>
            </div>
            <form method="GET" action="{{route('result')}}">
                    <input type="search" class="form-control mr-sm-2" name="search_query" placeholder="Search">
            </form>
            <div class="header-right">
                <a href="{{url('/upload')}}"><btn class="btn btn-primary">+ Upload Video</btn></a>
                <div class="dashboard">
                    <div class="sub">
                        <div class="sub-navlink"><a>Home</a></div>
                        <div class="sub-navlink"><a>Upload</a></div>
                        <div class="sub-navlink"><a>Dashboard</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-content">
            <div class="left-content">
                <div class="purple-text"><h1>Followed Channels</h1></div>
                <div class="channels">
                    <div class="channel">
                        <div class="profile"></div>
                        <p>Alfian's Channel</p>
                    </div>
                    <div class="channel">
                        <div class="profile"></div>
                        <p>Alfian's Channel</p>
                    </div>
                    <div class="channel">
                        <div class="profile"></div>
                        <p>Alfian's Channel</p>
                    </div>
                </div>
            </div>
            <div class="right-content">
                <div class="purple-text"><h1>Results</h1></div>
                @foreach ($videoLists->items as $key => $item)
                    <div class="card mb-4">
                        <a href="{{route('watch',$item->id->videoId)}}">
                            <img src="{{$item->snippet->thumbnails->medium->url}}" class="img-fluid">
                            <div class="body">
                                <h5 class="title">{{\Illuminate\Support\Str::limit($item->snippet->title,$limit = 200, $end='...')}}</h5>
                            </div>
                            <div class="footer text-muted">
                                Published at {{date('d M Y'), strtotime($item->snippet->publishTime)}}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </body>
</html>