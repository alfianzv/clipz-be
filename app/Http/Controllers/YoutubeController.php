<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class YoutubeController extends Controller
{
    public function index(){
        if(session('search_query')){
            $videoLists = $this->_videoLists('search_query');
        }
        else{
            $videoLists = $this->_videoLists('superman');
        }
        return view('debug.home', compact('videoLists'));
    }

    public function upload(){
        return view('debug.upload');
    }

    public function result(Request $request){
        session(['search_query' => $request->search_query]);
        $videoLists = $this->_videoLists($request->search_query);
        return view('debug.result',compact('videoLists'));
    }

    public function watch($id){
        $singleVideo = $this->_singleVideo($id);
        if (session('search_query')) {
            $videoLists = $this->_videoLists(session('search_query'));
        } else {
            $videoLists = $this->_videoLists('superman');
        }
        return view('debug.watch', compact('singleVideo','videoLists'));
    }

    protected function _videoLists($keyword){
        $part = 'snippet';
        $country = 'BD';
        $apiKey = config ('services.youtube.api_key');
        $maxResults = 10;
        $youTubeEndPoint = config('services.youtube.search_endpoint');
        $type = 'video,playlist,channel';

        $url = "$youTubeEndPoint?part=$part&maxResults=$maxResults&regionCode=$country&type=$type&key=$apiKey&q=$keyword";
        $response = Http::get($url);
        $results = json_decode($response);
        File::put(storage_path() . '/app/public/results.json', $response->body());

        return $results;
    }

    protected function _singleVideo($id)
    {
        $apiKey = config('services.youtube.api_key');
        $part = 'snippet';
        $url = "https://www.googleapis.com/youtube/v3/videos?part=$part&id=$id&key=$apiKey";
        $response = Http::get($url);
        $results = json_decode($response);

        // Will create a json file to see our single video details
        File::put(storage_path() . '/app/public/single.json', $response->body());
        return $results;
    }
}
